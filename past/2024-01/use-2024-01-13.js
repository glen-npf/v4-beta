/*---------------------------------------------------------

    NPF images fix v4.0 by @glenthemes [2023]
    github.com/npf-images-v4

    > v4.0.0 - 2023-12-14
    > Last updated: 2023-12-31 5:09PM [PST]
    
    CHANGE LOG (old -> new):
    > issue scope: modern captions
      issue details: cptless npf would delete comments
      example post: glen-test: 737458089298722816
    > issue scope: row/column heights
      issue details: instantiated img loads incorrectly
      
---------------------------------------------------------*/

window.NPFv4 = () => {
  const vpw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
  
  // remove <p>s that only have a <br> in them (no text)
  document.querySelectorAll("[post-type='text'] p > br:only-child")?.forEach(br => {
    if(br.parentNode.textContent.trim() == ""){
      br.parentNode.remove()
    }
  })
  
  // remove empty <p>s
  document.querySelectorAll("[post-type='text'] p ")?.forEach(p => {
    if(p.innerHTML.trim() == ""){
      p.remove()
    }
  })

  /*--------- NPF BASIC SETUP ---------*/
  // wrap neighboring .tmblr-full (parent: .npf_row) in .npf_col
  document.querySelectorAll(".npf_row .tmblr-full:not(:only-of-type)")?.forEach(n => {
    if(!n.parentNode.matches(".npf_col")){
      let npf_col = document.createElement("div");
      npf_col.classList.add(".npf_col");
      n.before(npf_col);
      npf_col.append(n)
    }
  })

  // wrap parentless .tmblr-full (video)
  document.querySelectorAll(`[data-npf*='"type":"video"']`)?.forEach(t => {
    if(!t.closest(".npf_inst")){
      let inst = document.createElement("div");
      inst.classList.add("npf_inst");
      inst.classList.add("npf_video");
      t.before(inst);
      inst.append(t)
    }
  })
  
  // wrap parentless .tmblr-full (audio)
  document.querySelectorAll(`[data-npf*='"type":"audio"']`)?.forEach(t => {
    if(!t.closest(".npf_inst")){
      let inst = document.createElement("div");
      inst.classList.add("npf_inst");
      inst.classList.add("npf_audio");
      t.before(inst);
      inst.append(t)
    }
  })
  
  // wrap parentless .tmblr-full (normal)
  document.querySelectorAll(".tmblr-full:not(.npf_row .tmblr-full)")?.forEach(t => {
    if(!t.closest(".npf_inst")){
      let npf_row = document.createElement("div");
      npf_row.classList.add("npf_row");
      t.before(npf_row);
      npf_row.append(t)
    }
  })

  // check if rows contain non-image media
  document.querySelectorAll(".npf_row:not(.npf_inst .npf_row)")?.forEach(n => {
    // video
    if(n.querySelector("video") || n.querySelector(`[data-npf*='"type":"video"']`)){
      let npf_inst = document.createElement("div");
      npf_inst.classList.add("npf_inst");
      npf_inst.classList.add("npf_video");
      n.before(npf_inst);
      npf_inst.append(n)
    }

    // audio
    else if(n.querySelector("audio") || n.querySelector(`[data-npf*='"type":"audio"']`)){
      let npf_inst = document.createElement("div");
      npf_inst.classList.add("npf_inst");
      npf_inst.classList.add("npf_audio");
      n.before(npf_inst);
      npf_inst.append(n)
    }
  })

  // wrap .npf_rows into a .npf_inst
  document.querySelectorAll(".npf_row")?.forEach(npf_row => {
    let hasPrev = npf_row.previousElementSibling;
    if(!hasPrev || (hasPrev && !hasPrev.matches(".npf_row"))){
      let npf_inst = document.createElement("div");
      npf_inst.classList.add("npf_inst");
      npf_row.before(npf_inst);

      let next = npf_inst.nextElementSibling;
      while(next && next.matches(".npf_row")){
        npf_inst.append(next);
        next = npf_inst.nextElementSibling
      }
    }
  })

  // redo npf_inst wrapping just in case
  // i.e. eliminate cases of .npf_inst + .npf_inst
  document.querySelectorAll(".npf_inst:not(.npf_video, .npf_audio)")?.forEach(n => {
    let next = n.nextElementSibling;
    if(next){
      while(next && next.matches(".npf_inst:not(.npf_video, .npf_audio)")){
        n.append(next);
        next.replaceWith(...next.childNodes);
        next = n.nextElementSibling
      }
    }
  })

  // deal with .npf_inst inside .npf_inst
  document.querySelectorAll(".npf_inst .npf_inst")?.forEach(n => {
    n.replaceWith(...n.childNodes)
  })

  // group .npf_insts
  document.querySelectorAll(".npf_inst:not(.npf_group .npf_inst)")?.forEach(n => {
    let prev = n.previousElementSibling;
    if(!prev || (prev && !prev.matches(".npf_inst"))){
      let group = document.createElement("div");
      group.classList.add("npf_group");
      n.before(group);

      let next = group.nextElementSibling;
      if(next){
        while(next && next.matches(".npf_inst")){
          group.append(next);
          next = group.nextElementSibling
        }
      }
    }
  })

  // display number of cols
  document.querySelectorAll(".npf_row")?.forEach(n => {
    if(n.querySelector(".npf_col")){
      let cols = n.querySelectorAll(".npf_col").length;
      n.setAttribute("columns",cols)
    } else {
      n.setAttribute("columns","1")
    }
  })
  
  /*--------- NPF LIGHTBOX FUNCTIONALITY ---------*/  
  // enable new object creation
  function npfIMG(w, h, ld, hd){
    this.width = Number(w);
    this.height = Number(h);
    this.low_res = ld;
    this.high_res = hd;
  }

  document.querySelectorAll(".npf_inst:not(.npf_video, .npf_audio)")?.forEach(npf_inst => {
    // collect all images in each instance
    if(npf_inst.querySelector("img")){
      let imgs = [];

      npf_inst.querySelectorAll("img").forEach((img, imgIndex) => {

        // remove single photo lightbox if it has one
        let a = img.closest("a.post_media_photo_anchor");
        if(a && a.matches("[data-big-photo]")){
          a.removeAttribute("data-big-photo")
        }

        let imgW, imgH, lowRES, highRES;

        // if YES SRCSET
        if(img.matches("[srcset]")){
          let srcset = img.getAttribute("srcset");          
          lowRES = img.src;
          highRES = srcset.split(",").pop().trim().split(" ")[0];

          let ljsze = new Image();
          ljsze.src = highRES;
          imgW = ljsze.width;
          imgH = ljsze.height;

          if(imgW >= vpw*0.9){
            imgW *= 0.7
            imgH *= 0.7
          }
        }

        // if NO SRCSET
        else {
          lowRES = img.src;
          highRES = img.src;

          let bigPhotoW = img.closest("[data-big-photo-width]");
          let bigPhotoH = img.closest("[data-big-photo-height]");

          if(bigPhotoW){
            imgW = Number(bigPhotoW.getAttribute("data-big-photo-width"));
          } else {
            imgW = img.width;
          }

          if(bigPhotoH){
            imgH = Number(bigPhotoH.getAttribute("data-big-photo-height"));
          } else {
            imgH = img.height;
          }
        }

        imgs.push(new npfIMG(
            imgW, imgH, lowRES, highRES
        ));

        let nonZeroIndex = Math.floor(parseInt(imgIndex)+1);
        img.setAttribute("img-index",nonZeroIndex);
        
        npf_inst.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(imgs)},1)`)

        img.addEventListener("click", () => {
          npf_inst.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(imgs)},${nonZeroIndex})`)
        })
      })//end <img> forEach

      // console.log(imgs)
    }//end: npf_inst has imgs
  })

  /*--------- PREPEND 1ST PHOTOSET ---------*/  
  let moveOpt = getComputedStyle(document.documentElement).getPropertyValue("--NPF-Move-1st-Photoset");

  if(moveOpt.trim() !== '' && (moveOpt.trim() == 'yes' || moveOpt.trim() === '"yes"' || moveOpt.trim() === "'yes'")){

    // reblog list ID (block:Text container)
    let hasCaption = "";
    let captionName = getComputedStyle(document.documentElement).getPropertyValue("--Text-Container-Selector");
    if(captionName.trim() !== ""){
      captionName = captionName.trim();
      if(captionName.slice(0,1) == '"'){
        captionName = captionName.slice(1)
      }

      if(captionName.slice(-1) == '"'){
        captionName = captionName.slice(0,-1)
      }
      
      if(captionName.trim() !== ""){
        let captz = document.querySelector(captionName);
        captz ? hasCaption = "true" : null;
      } else {
        hasCaption = "false"
      }
    }

    // each reblog entry ID
    let hasEntry = "";
    let entryName = getComputedStyle(document.documentElement).getPropertyValue("--Text-Reblogs-Selector");
    if(entryName.trim() !== ""){
      entryName = entryName.trim();
      if(entryName.slice(0,1) == '"'){
        entryName = entryName.slice(1)
      }

      if(entryName.slice(-1) == '"'){
        entryName = entryName.slice(0,-1)
      }
      
      if(entryName.trim() !== ""){
        let entryz = document.querySelector(entryName);
        entryz ? hasEntry = "true" : null;
      } else {
        hasEntry = "false"
      }
    }
    
    /*--------- UNNESTED CAPTIONS [neo, bev] ---------*/
    let nest_status = "";
    if(window.jQuery || window.$){
      if($().unnest){
        nest_status = "true"
      } else {
        nest_status = "false"
      }
    }
    
    if(nest_status == "true"){
      document.querySelectorAll("[post-type='text']:not(.npf-loaded)")?.forEach(textPost => {
        let rqxpe = Date.now();
        let jlpea = 2000;
        let snxon = setInterval(() => {
          if(Date.now() - rqxpe > jlpea){
            clearInterval(snxon)
          } else {
            let tumblr_parent = textPost.querySelector("blockquote[class]:not([class=''])");
            if(tumblr_parent){
              clearInterval(snxon)
              
              let otherReblogs = tumblr_parent.nextElementSibling;
              if(otherReblogs && otherReblogs.matches("blockquote[class]:not([class=''])")){
                otherReblogs = "true"
              } else {
                otherReblogs = "false"
              }
              
              let user_a = tumblr_parent.querySelector(":scope > a.tumblr_blog");
              if(user_a){
                let npfFirst = user_a.nextElementSibling;
                if(npfFirst && npfFirst.matches(".npf_group")){
                  let znext = npfFirst.nextElementSibling;
                  // no caption
                  if(!znext || znext && znext.innerHTML.trim() == ""){
                    tumblr_parent.classList.add("tbd");
                    npfFirst.classList.add("photo-origin");
                    tumblr_parent.before(npfFirst);
                    tumblr_parent.remove();
                    
                    if(hasCaption == "true"){
                      let capt = textPost.querySelector(captionName);
                      if(capt){
                        capt.classList.add("npf-caption");
                        capt.before(npfFirst);
                        
                        if(otherReblogs == "false"){
                          capt.remove()
                        }
                      }
                    }
                  }
                  
                  // has caption
                  else {
                    npfFirst.classList.add("photo-origin");
                    tumblr_parent.before(npfFirst);
                    
                    if(hasCaption == "true"){
                      let capt = textPost.querySelector(captionName);
                      if(capt){
                        capt.classList.add("npf-caption");
                        capt.before(npfFirst);
                        npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";
                        capt.classList.add("npf-caption")
                      }
                    }
                  }
                }
              }
            }
          }
        },1)
        
        textPost.classList.add("npf-loaded")
      })
    }
    
    else {
      document.querySelectorAll("[post-type='text']:not(.npf-loaded)")?.forEach(textPost => {
        let leg = textPost.querySelector("p:first-child > a.tumblr_blog[href]:first-child");
        if(leg && leg.parentNode.nextElementSibling && leg.parentNode.nextElementSibling.matches("blockquote:not([class])")){
          /*--------- OLD BLOCKQUOTE CAPTIONS ---------*/        
          let lastComment = leg.parentNode;
          lastComment.setAttribute("last-comment","");

          textPost.querySelectorAll("p:first-child > a.tumblr_blog[href]:first-child")?.forEach(a => {
            a.parentNode.setAttribute("username","")
          })

          textPost.querySelectorAll("p[username] + blockquote")?.forEach(b => {
            b.setAttribute("said","")
          })

          textPost.querySelectorAll("p[username] + blockquote[said]")?.forEach(b => {
            if(!b.querySelector("p[username]")){
              // ORIGINAL ENTRY
              let originalEntry = b;
              let npfFirst = originalEntry.querySelector(".npf_group");
              if(npfFirst){
                let prev = npfFirst.previousElementSibling;
                if(!prev || (prev && prev.innerHTML.trim() == "")){
                  // CAN RELOCATE
                  npfFirst.classList.add("photo-origin");

                  let next = npfFirst.nextElementSibling;
                  if(!next || (next && next.innerHTML.trim() == "")){
                    // NO CAPTION
                    originalEntry.classList.add("tbd")
                    originalEntry.previousElementSibling.classList.add("tbd");                  
                  }

                  npfFirst.closest("[post-type='text']").querySelector("p[last-comment]").before(npfFirst);

                  // only 1 comment
                  let nom = npfFirst.nextElementSibling;
                  if(nom.matches(".tbd")){
                    if(nom.nextElementSibling.matches(".tbd")){
                      nom.nextElementSibling.remove()
                      nom.remove();
                    }
                  }

                  // multiple comments
                  else {
                    nom = npfFirst.nextElementSibling.nextElementSibling;
                    if(nom.matches("blockquote")){
                      nom.querySelectorAll(".tbd")?.forEach(tbd => {
                        tbd.remove()
                      })
                    }
                  }

                }
              }
            }
          })//end: find deepest bq (op)

          textPost.querySelectorAll("p[username], blockquote[said]")?.forEach(e => {
            e.removeAttribute("username");
            e.removeAttribute("said");
          })
        }//end: OLD BLOCKQUOTE CAPTIONS

        else {
          /*--------- MODERN CAPTIONS ---------*/

          if(hasEntry == "true"){
            if(textPost.querySelector(entryName)){
              // loop through entries
              textPost.querySelectorAll(entryName)?.forEach((entry, i) => {
                // focus on the 1st entry, the original entry
                if(i == 0){
                  setTimeout(() => {
                    // .npf_inst: first
                    let npfFirst = entry.querySelector(".npf_group"); // already first instance

                    if(npfFirst){
                      /*---- NEXT ----*/
                      let npfNext = npfFirst.nextElementSibling;
                      if(npfNext){
                        if(npfNext.innerHTML.trim() !== ""){
                          // has caption
                          entry.classList.add("npf-caption")
                        } else {
                          // no caption
                          entry.classList.add("tbd")
                        }
                      } else {
                        // no caption
                        entry.classList.add("tbd")
                      }
                      
                      /*---- PREVIOUS ----*/
                      let npfPrev = npfFirst.previousElementSibling;
                      if(npfPrev){
                        if(npfPrev.matches("a[href*='.tumblr.com/post']") || npfPrev.matches("a.user") || npfPrev.matches("a.username") || npfPrev.matches("a[target='_blank']") || (npfPrev.matches("p") && npfPrev.innerHTML.trim() == "")){
                          moveIt()
                        }
                        
                        else if(npfPrev.matches("p")){
                          if(npfPrev.innerHTML.trim() == ""){
                            moveIt()
                          }
                        }
                        
                        else {
                          // do nothing
                        }
                      } else {
                        moveIt()
                      }
                      
                      function moveIt(){
                        npfFirst.classList.add("photo-origin");
                        entry.before(npfFirst);
                        if(!entry.matches(".tbd")){
                          npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";
                        }
                        
                        if(hasCaption == "true"){
                          let findCapt = entry.closest(`[post-type="text"]`).querySelector(captionName);
                          findCapt.before(npfFirst);
                          if(entry.matches(".tbd")){
                            // findCapt.remove()
                            let checkNext = entry.nextElementSibling;
                            
                            // if theres a .next() but it's not another reblog/comment
                            if(checkNext){
                              if(!checkNext.matches(entryName)){
                                findCapt.remove()
                              }
                            }
                            
                            // no .next()
                            else {
                              findCapt.remove();
                            }
                            
                            entry.remove();
                          }
                        } else {
                          if(entry.matches(".tbd")){
                            entry.remove()
                          }
                        }
                      }
                    }//end: npfFirst exists
                  },0)
                }//end: original entry

              })
            }//end: $entryName div exists
          }//end: if entryName is not empty
        }//end: MODERN CAPTIONS
        
        textPost.classList.add("npf-loaded")
        
      })//end [post-type="text"] forEach
    }//end: anything thats NOT neobev unnest
    
  }//end: YES, PREPEND IT
}//end NPFv4

/*--------- NPF ROW HEIGHT ---------*/
window.npfRowHeight = () => {
  document.querySelectorAll(".npf_row[columns]:not([columns='1'], [columns=''], [columns='0'])")?.forEach(row => {
    // TEMPORARY HEIGHT
    let cols = Array.from(row.querySelectorAll(":scope > .npf_col img:first-of-type"));
    let minHeight = Math.min(...cols.map(col => col.offsetHeight));
    cols.forEach(col => col.closest(".npf_col").style.height = `${minHeight}px`);
    
    // HOW MANY COLUMNS IN THAT ROW?
    let lens = row.querySelectorAll(":scope > .npf_col").length;
    
    // NATURAL DIMENSIONS (ORIGINAL DIMENSIONS)
    let NATURAL_WIDTHS = [];
    let NATURAL_HEIGHTS = [];
    
    // CURRENT DIMENSIONS
    let OFFSET_WIDTHS = [];
    let OFFSET_HEIGHTS = [];
    
    // COLLECT NATURAL-TO-CURRRENT WIDTH RATIO DIFFS
    let W_RATIOS = [];
    
    // COLLECT HEIGHT OF EACH CELL (COLUMN) HEIGHT OF THAT ROW
    let CELL_HEIGHTS = [];

    // height after img confirmed loaded
    row.querySelectorAll(":scope > .npf_col img:first-of-type")?.forEach(img => {
      let pseudoIMG = new Image();
      pseudoIMG.src = img.src;
      pseudoIMG.addEventListener("load", (event) => {
        let w = event.target.naturalWidth;
        let h = event.target.naturalHeight;
        NATURAL_WIDTHS.push(w);
        NATURAL_HEIGHTS.push(h);
      })
      
      OFFSET_WIDTHS.push(img.offsetWidth);
      OFFSET_HEIGHTS.push(img.offsetHeight);
    })
    
    let x = Date.now();
    let y = 2500;
    let z = setInterval(() => {
      if(Date.now - x > y){
        clearInterval(z)
      } else {
        // only do the math thing when the images in THAT ROW have all loaded
        if(NATURAL_WIDTHS.length == lens){
          clearInterval(z);
          for(let i=0; i<NATURAL_WIDTHS.length; i++){
            W_RATIOS.push(OFFSET_WIDTHS[i] / NATURAL_WIDTHS[i]);
          }
          
          row.querySelectorAll(":scope > .npf_col img")?.forEach((img, i) => {
            let genH = NATURAL_HEIGHTS[i] * W_RATIOS[i];
            CELL_HEIGHTS.push(genH)
          });
          
          let shortest = Math.min(...CELL_HEIGHTS);
          row.querySelectorAll(":scope > .npf_col")?.forEach(col => {
            col.style.height = `${shortest}px`;
          })
        }
      }
    },0)
  })
}

document.addEventListener("DOMContentLoaded", () => {  
  NPFv4();
  npfRowHeight();
  
  if(window.jQuery || window.$){
    if($().infinitescroll){
      let hi = Math.max(document.documentElement.scrollHeight || 0, document.body.scrollHeight || 0);
      window.addEventListener("scroll", () => {
        let height_observed = Math.max(document.documentElement.scrollHeight || 0, document.body.scrollHeight || 0);
        let diff = height_observed - hi;
        if(diff > 100){
          NPFv4();
          npfRowHeight()
        }
      })
    }
  }
})

window.addEventListener("load", () => {
  npfRowHeight();
})

window.addEventListener("resize", () => {
  npfRowHeight();
})
