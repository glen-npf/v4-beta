### Tumblr NPF Images Fix (beta)

**Author:** @&#x200B;glenthemes  
**Description:** Cleans up the appearance of NPF images on Tumblr themes and (optionally) moves the main photo (or photoset) outside of its text container.  
**Version:** 4.0.2  
**Last updated:** 2024-12-21 07:31&#x200A;PM PDT

**Warning:**
* This is still in beta / unreleased; unexpected issues may arise.
* Constantly under edits and improvements.\
Changelog [here](https://glen-npf.gitlab.io/v4-beta/CHANGELOG).

#### Table of Contents:
- [What is NPF?](#what-is-npf)
- [Features](#features)
- [Limitations](#limitations)
- [How to install](#step-1-of-3)
- [Usage notes](#usage-notes)
- [Further support](#need-help)

---

#### What is NPF?
NPF stands for "Neue Post Format". Tumblr used to have multiple types of post formats to choose from (namingly: text, photo, quote, link, chat, video, questions), but in recent years they've started to turn many of those formats into NPF only (in other words, everything becomes a text post). This means that all content uploaded via Tumblr mobile turn into NPF posts. NPF images can also refer to images between paragraphs.

As of 2023, the NPF update affects all users and new posts they create.

---

### Features:
- turns images HD if available
- prevents images from stretching, squashing, and overflowing
- custom spacing between images
- custom spacing between images and captions
- photoset lightbox support
- [optional] repositions main photoset to top of post if they were meant to look like photo posts (e.g. for artists), in which the photoset is the first item in the caption.
- removes the blockquote border from the first photoset if it has one.

---

#### Limitations:
This fix *attempts* to be easily installable & one-size-fits-all, but likely needs HTML and [Tumblr docs](https://www.tumblr.com/docs/en/custom_themes) knowledge to make it work as intended, since the markup/structure of every theme is different depending on how the theme maker set it up.

---

#### Step 1 of 3:

Paste the following under `<head>` in your theme code:

```html
<!--
        NPF images fix v4.0 by @glenthemes [2023]
        💌 gitlab.com/glen-npf/v4-beta
--->
<link href="//glen-npf.gitlab.io/v4-beta/main.css" rel="stylesheet">
<script src="//glen-npf.gitlab.io/v4-beta/use.js"></script>
<style>
:root {
    --NPF-Images-Spacing:4px;
    --NPF-Caption-Spacing:1em;
    --NPF-Move-1st-Photoset:"yes";
    --Text-Container-Selector:".caption";
    --Text-Reblogs-Selector:".tumblr_parent";
}
</style>
```

---

#### Step 2 of 3:

Next, bring up the searchbar by clicking the **gear icon** > **Find and replace**:

![](https://file.garden/ZRt8jW_MomIqlrfn/screenshots/plkxe.png)

Type `{block:Posts` into the first searchbar field to see if it exists. You can use the accompanying arrow keys to check if there's more than one result:

![](https://file.garden/ZRt8jW_MomIqlrfn/screenshots/12348929847.png)

If you find `{block:PostSummary}`, ignore that.

Find either `{block:Posts}` or `{block:Posts inline` and replace that line with this:
```js
{block:Posts inlineMediaWidth="1280" inlineNestedMediaWidth="1280"}
```

On the following line or so, add `post-type="{PostType}"` to the end, before the closing pointy bracket `>`, e.g.:
```html
<div class="posts" post-type="{PostType}">
```

If you're not sure which line to put it on, use the one with `{PostID}` if it has one.

---

#### Step 3 of 3:

You can configure your NPF options here.

| Option name | Explanation |
| ------ | ------ |
| `--NPF-Images-Spacing` | Gaps between NPF images.<br/>Example: `4px` |
| `--NPF-Caption-Spacing` | Gap between an NPF photoset and its caption text.<br/>Example: `1em` or `16px` |
| `--NPF-Move-1st-Photoset` | `"yes"` takes the first NPF photoset in a post and moves it outside of its text container.<br/><br/>`"no"` does not move the NPF photoset. |
| `--Text-Container-Selector` | The selector name for text posts.<br/><br/>Common text container selector names are `".caption"`, `".capt`, `".cpt`, `".text"`, `".txt"`, or `".tex"`. |
| `--Text-Reblogs-Selector` | The selector name of each comment entry *within the text post*, including the original one. If your theme includes a username or their profile picture, it should be included inside here.<br/><br/>Using [this post](https://www.tumblr.com/devsmaycry/186738981199) as an example:<br/>![](https://file.garden/ZRt8jW_MomIqlrfn/screenshots/zllgc.png)<br/> **@claude-money**'s post is one entry, and **@odetocody**'s post is another entry.<br/><br/>Common entry selector names are `".tumblr_parent"` or `".comment"`.  |

#### Usage notes:
If you have trouble finding what the selector names for `--Text-Container-Selector` and `--Text-Reblogs-Selector` are, you can leave them blank, like so:
```css
:root {
    --NPF-Images-Spacing:4px;
    --NPF-Caption-Spacing:1em;
    --NPF-Move-1st-Photoset:"yes";
    --Text-Container-Selector:"";
    --Text-Reblogs-Selector:"";
}
```

<details>
<summary>
Click for tips on finding your selectors!
</summary>

<br/>

Both `--Text-Container-Selector` and `--Text-Reblogs-Selector` can be found under `{block:Text}` in your theme code, where NPF posts (basically text posts) are rendered.
- the line of HTML immediately below `{block:Text}` is usually the `--Text-Container-Selector`.
- if `{block:Reblogs}` exists, the line below it is usually the `--Text-Reblogs-Selector`.
- if there is no `{block:Reblogs}`, anything encompassing `{Body}` is usually the `--Text-Reblogs-Selector`.
- finding the `--Text-Reblogs-Selector` is more important than finding the `--Text-Container-Selector`.

**Example 1: basic structure A:**
(typically found on older themes)
```html
{block:Text}
  {block:Title}<h2>{Title}</h2>{/block:Title}
  <div class="caption">{Body}</div>
{/block:Text}
```
In this example:
- `--Text-Container-Selector` does not exist, leave it blank.
- `--Text-Reblogs-Selector` name is `".caption"`

**Example 2: basic structure B:**
(typically found on older themes)
```html
{block:Text}
  <div class="text">
    {block:Title}<h2>{Title}</h2>{/block:Title}
    <div class="caption">{Body}</div>
  </div>
{/block:Text}
```
In this example:
- `--Text-Container-Selector` name is `".text"`
- `--Text-Reblogs-Selector` name is `".caption"`

**Example 3: basic structure C:**
(typically found on older themes)
```html
{block:Text}
  {block:Title}<h2>{Title}</h2>{/block:Title}

  <div class="text">
    <div class="caption">{Body}</div>
  </div>
{/block:Text}
```
In this example:
- `--Text-Container-Selector` does not exist, leave it blank.
- `--Text-Reblogs-Selector` name is `".text"`

**Example 4: complex structure A:**
(typically found on newer themes)
```html
{block:Text}
  <div class="caption">
  {block:Title}<h2>{Title}</h2>{/block:Title}

  {block:RebloggedFrom}
    {block:Reblogs}
      <div class="comment">{Body}</div>
    {/block:Reblogs}
  {/block:RebloggedFrom}

  {block:NotReblog}
    <div class="comment">{Body}</div>
  {/block:NotReblog}
  </div>
{/block:Text}
```
In this example:
- `--Text-Container-Selector` name is `".caption"`
- `--Text-Reblogs-Selector` name is `".comment"`

**Example 5: complex structure B:**
(typically found on newer themes)
```html
{block:Text}
  <div class="caption">
  {block:Title}<h2>{Title}</h2>{/block:Title}

  {block:RebloggedFrom}
    {block:Reblogs}
      <div class="comment_container">
        <div class="comment-header"></div>
        <div class="comment">{Body}</div>
      </div>      
    {/block:Reblogs}
  {/block:RebloggedFrom}

  {block:NotReblog}
    <div class="comment_container">
      <div class="comment-header"></div>
      <div class="comment">{Body}</div>
    </div>
  {/block:NotReblog}
  </div>
{/block:Text}
```
In this example:
- `--Text-Container-Selector` name is `".caption"`
- `--Text-Reblogs-Selector` name is `".comment_container"`

**Example 6: complex structure C:**
(typically found on newer themes)
```html
{block:Text}
  {block:Title}<h2>{Title}</h2>{/block:Title}

  {block:RebloggedFrom}
    {block:Reblogs}
      <div class="caption">
        <div class="comment">{Body}</div>
      </div>
    {/block:Reblogs}
  {/block:RebloggedFrom}

  {block:NotReblog}
    <div class="caption">
      <div class="comment">{Body}</div>
    </div>
  {/block:NotReblog}
{/block:Text}
```
In this example:
- `--Text-Container-Selector` does not exist, leave it blank.
- `--Text-Reblogs-Selector` name is `".caption"`
</details>

---

#### Additional notes:
If you've set `NPF-Move-1st-Photoset` to `"yes"` but the photoset still isn't repositioning itself, make sure your `{Body}` segment in your text block is wrapped in a `div` or similar:

**Example, not wrapped ❌:**
```html
<div class="reblogs">
  <span class="username">username text</span>
  {Body}
</div>
```

**Example, wrapped ✅:**
```html
<div class="reblogs">
  <span class="username">username text</span>
  <div>{Body}</div>
</div>
```

---

#### Need help?

To ask questions:
1. Join my Discord server: [discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)
2. Read and tick the `#server-rules`
3. Send a message (with the required information) in either `#theme-help` or `#open-questions`!
