/*-----------------------------------------------------------------

    NPF images fix v4.0 by @glenthemes [2023]
    gitlab.com/glen-npf/v4-beta

    > v4.0.2 - 2024-12-21
    > Last updated: 2024-12-21 6:34PM [PST]
    > Change log: glen-npf.gitlab.io/v4-beta/CHANGELOG
      
-----------------------------------------------------------------*/

window.NPFv4 = () => {
  const vpw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
  
  document.querySelectorAll("[post-type='text'] p > br:only-child")?.forEach(br => {
    let p = br.closest("p");
    Array.from(p.childNodes)?.forEach(node => {
      if(node.nodeType === 3 && node.data.trim().length){
        let span = document.createElement("span");
        node.before(span);
        span.appendChild(node);
      }
    })
  })
  
  // remove empty <p>s
  document.querySelectorAll("[post-type='text'] p ")?.forEach(p => {
    let o = p.outerHTML.trim();
    if(o === "<p></p>"){
      p.remove()
    }
  })

  /*--------- NPF BASIC SETUP ---------*/
  // go through all CLASSLESS <figure>s and check if they're supposed to be .tmblr-full
  document.querySelectorAll("[post-type] figure:not(.tmblr-full)")?.forEach(fig => {
    if(fig.querySelector("img")){
      fig.classList.add("tmblr-full")
    }
  })
  
  // wrap neighboring .tmblr-full (parent: .npf_row) in .npf_col
  document.querySelectorAll(".npf_row .tmblr-full:not(:only-of-type)")?.forEach(n => {
    if(!n.parentNode.matches(".npf_col")){
      let npf_col = document.createElement("div");
      npf_col.classList.add("npf_col");
      n.before(npf_col);
      npf_col.append(n)
    }
  })

  // wrap parentless .tmblr-full (video)
  document.querySelectorAll(`[data-npf*='"type":"video"']`)?.forEach(t => {
    if(!t.closest(".npf_inst")){
      let inst = document.createElement("div");
      inst.classList.add("npf_inst");
      inst.classList.add("npf_video");
      t.before(inst);
      inst.append(t)
    }
  })
  
  // wrap parentless .tmblr-full (audio)
  document.querySelectorAll(`[data-npf*='"type":"audio"']`)?.forEach(t => {
    if(!t.closest(".npf_inst")){
      let inst = document.createElement("div");
      inst.classList.add("npf_inst");
      inst.classList.add("npf_audio");
      t.before(inst);
      inst.append(t)
    }
  })
  
  // wrap parentless .tmblr-full (normal)
  document.querySelectorAll(".tmblr-full:not(.npf_row .tmblr-full)")?.forEach(t => {
    if(!t.closest(".npf_inst")){
      let npf_row = document.createElement("div");
      npf_row.classList.add("npf_row");
      t.before(npf_row);
      npf_row.append(t)
    }
  })

  // check if rows contain non-image media
  document.querySelectorAll(".npf_row:not(.npf_inst .npf_row)")?.forEach(n => {
    // video
    if(n.querySelector("video") || n.querySelector(`[data-npf*='"type":"video"']`)){
      let npf_inst = document.createElement("div");
      npf_inst.classList.add("npf_inst");
      npf_inst.classList.add("npf_video");
      n.before(npf_inst);
      npf_inst.append(n)
    }

    // audio
    else if(n.querySelector("audio") || n.querySelector(`[data-npf*='"type":"audio"']`)){
      let npf_inst = document.createElement("div");
      npf_inst.classList.add("npf_inst");
      npf_inst.classList.add("npf_audio");
      n.before(npf_inst);
      npf_inst.append(n)
    }
  })

  // wrap .npf_rows into a .npf_inst
  document.querySelectorAll(".npf_row")?.forEach(npf_row => {
    let hasPrev = npf_row.previousElementSibling;
    if(!hasPrev || (hasPrev && !hasPrev.matches(".npf_row"))){
      let npf_inst = document.createElement("div");
      npf_inst.classList.add("npf_inst");
      npf_row.before(npf_inst);

      let next = npf_inst.nextElementSibling;
      while(next && next.matches(".npf_row")){
        npf_inst.append(next);
        next = npf_inst.nextElementSibling
      }
    }
  })

  // redo npf_inst wrapping just in case
  // i.e. eliminate cases of .npf_inst + .npf_inst
  document.querySelectorAll(".npf_inst:not(.npf_video, .npf_audio)")?.forEach(n => {
    let next = n.nextElementSibling;
    if(next){
      while(next && next.matches(".npf_inst:not(.npf_video, .npf_audio)")){
        n.append(next);
        next.replaceWith(...next.childNodes);
        next = n.nextElementSibling
      }
    }
  })

  // deal with .npf_inst inside .npf_inst
  document.querySelectorAll(".npf_inst .npf_inst")?.forEach(n => {
    n.replaceWith(...n.childNodes)
  })

  // group .npf_insts
  document.querySelectorAll(".npf_inst:not(.npf_group .npf_inst)")?.forEach(n => {
    let prev = n.previousElementSibling;
    if(!prev || (prev && !prev.matches(".npf_inst"))){
      let group = document.createElement("div");
      group.classList.add("npf_group");
      n.before(group);

      let next = group.nextElementSibling;
      if(next){
        while(next && next.matches(".npf_inst")){
          group.append(next);
          next = group.nextElementSibling
        }
      }
    }
  })
  
  // go through each .npf_group > .npf_inst(:first)
  // [1] see if first item only has 1 col
  // [2] see if that is an image
  // [3] see if it's 300px wide and over (aka the tumblr threshold for not-enlarging images to post width)
  // reference: static.tumblr.com/gtjt4bo/fUqsl6252/inline-image-sizes.png
  let imgThresholdWidth = 300;
  
  document.querySelectorAll(".npf_group > .npf_inst:first-child")?.forEach(npf_inst1 => {
    let fc = npf_inst1.firstElementChild;
    if(fc && fc.matches(".npf_row")){
      if(!fc.querySelector(".npf_col")){
        if(fc.querySelector("img")){
          let img = fc.querySelector("img");
          
          let imgWidth;
          if(img.closest("[data-big-photo-width]")){
            imgWidth = Number(img.closest("[data-big-photo-width]").getAttribute("data-big-photo-width"))
          } else {
            imgWidth = img.naturalWidth
          }
          
          if(!isNaN(imgWidth) && imgWidth < imgThresholdWidth){
            npf_inst1.closest(".npf_group").classList.add("npf-small");
          }
        }
      }
    }
  })

  // display number of cols
  document.querySelectorAll(".npf_row")?.forEach(n => {
    if(n.querySelector(".npf_col")){
      let cols = n.querySelectorAll(".npf_col").length;
      n.setAttribute("columns",cols)
    } else {
      n.setAttribute("columns","1")
    }
  })
	
	// get shortest height of row using either
	// aspect-ratio or getting the height
	// depending on what the browser supports
	aspectRatioOrUseHeight()
	
	/*-------  NPF COLUMNS: UNDO STRETCH -------*/
	document.querySelectorAll(".npf_row .npf_col")?.forEach(col => col.dataset.ready = "")
  
  /*--------- NPF LIGHTBOX FUNCTIONALITY ---------*/  
  // enable new object creation
  function npfIMG(w, h, ld, hd){
    this.width = Number(w);
    this.height = Number(h);
    this.low_res = ld;
    this.high_res = hd;
  }

  document.querySelectorAll(".npf_inst:not(.npf_video, .npf_audio)")?.forEach(npf_inst => {
    // collect all images in each instance
    if(npf_inst.querySelector("img")){
      npf_inst.classList.add("npf_photo");
      let imgs = [];

      npf_inst.querySelectorAll("img").forEach((img, imgIndex) => {

        // remove single photo lightbox if it has one
        let a = img.closest("a.post_media_photo_anchor");
        if(a && a.matches("[data-big-photo]")){
          a.removeAttribute("data-big-photo")
        }

        let imgW, imgH, lowRES, highRES;

        // if YES SRCSET
        if(img.matches("[srcset]")){
          let srcset = img.getAttribute("srcset");          
          lowRES = img.src;
          highRES = srcset.split(",").pop().trim().split(" ")[0];

          let ljsze = new Image();
          ljsze.src = highRES;
          imgW = ljsze.width;
          imgH = ljsze.height;

          if(imgW >= vpw*0.9){
            imgW *= 0.7
            imgH *= 0.7
          }
        }

        // if NO SRCSET
        else {
          lowRES = img.src;
          highRES = img.src;

          let bigPhotoW = img.closest("[data-big-photo-width]");
          let bigPhotoH = img.closest("[data-big-photo-height]");

          if(bigPhotoW){
            imgW = Number(bigPhotoW.getAttribute("data-big-photo-width"));
          } else {
            imgW = img.width;
          }

          if(bigPhotoH){
            imgH = Number(bigPhotoH.getAttribute("data-big-photo-height"));
          } else {
            imgH = img.height;
          }
        }

        imgs.push(new npfIMG(
            imgW, imgH, lowRES, highRES
        ));

        let nonZeroIndex = Math.floor(parseInt(imgIndex)+1);
        img.setAttribute("img-index",nonZeroIndex);
        
        npf_inst.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(imgs)},1)`)

        img.addEventListener("click", () => {
          npf_inst.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(imgs)},${nonZeroIndex})`);
        })
      })//end <img> forEach

      // console.log(imgs)
    }//end: npf_inst has imgs
  })//end: lightboxes

  /*--------- PREPEND 1ST PHOTOSET ---------*/  
  let moveOpt = getComputedStyle(document.documentElement).getPropertyValue("--NPF-Move-1st-Photoset");

  if(moveOpt.trim() !== '' && (moveOpt.trim() == 'yes' || moveOpt.trim() === '"yes"' || moveOpt.trim() === "'yes'")){

    // reblog list ID (block:Text container)
    let hasCaption = "false";
    let captionName = getComputedStyle(document.documentElement).getPropertyValue("--Text-Container-Selector");
    if(captionName.trim() !== ""){
      captionName = captionName.trim();
      if(captionName.slice(0,1) == '"'){
        captionName = captionName.slice(1)
      }

      if(captionName.slice(-1) == '"'){
        captionName = captionName.slice(0,-1)
      }
      
      if(captionName.trim() !== ""){
        let captz = document.querySelector(captionName);
        captz ? hasCaption = "true" : null;
      }
    }

    // each reblog entry ID
    let hasEntry = "false";
    let entryName = getComputedStyle(document.documentElement).getPropertyValue("--Text-Reblogs-Selector");
    if(entryName.trim() !== ""){
      entryName = entryName.trim();
      if(entryName.slice(0,1) == '"'){
        entryName = entryName.slice(1)
      }

      if(entryName.slice(-1) == '"'){
        entryName = entryName.slice(0,-1)
      }
      
      if(entryName.trim() !== ""){
        let entryz = document.querySelector(entryName);
        entryz ? hasEntry = "true" : null;
      }
    }
    
    // console logs
    // console.log(`hasCaption: ${hasCaption}, ${captionName}`)
    // console.log(`hasEntry: ${hasEntry}, ${entryName}`)
    
    /*--------- UNNESTED CAPTIONS [neo, bev] [CASE: 1] ---------*/
    let nest_status = "false";
    if(window.jQuery || window.$){
      if($().unnest){
        nest_status = "true"
      } else {
        nest_status = "false"
      }
    }
    
    if(nest_status == "true"){
      document.querySelectorAll("[post-type='text']:not(.npf-loaded)")?.forEach(textPost => {
        let rqxpe = Date.now();
        let jlpea = 2000;
        let snxon = setInterval(() => {
          if(Date.now() - rqxpe > jlpea){
            clearInterval(snxon)
          } else {
            setTimeout(() => {
              let tumblr_parent = textPost.querySelector("blockquote[class]:not([class=''], .npf_indented)");
              if(tumblr_parent){
                clearInterval(snxon);

                let otherReblogs = tumblr_parent.nextElementSibling;
                if(otherReblogs && otherReblogs.matches("blockquote[class]:not([class=''], .npf_indented)")){
                  otherReblogs = "true"
                } else {
                  otherReblogs = "false"
                }

                let user_a = tumblr_parent.querySelector(":scope > a.tumblr_blog");
                if(user_a){
                  let npfFirst = user_a.nextElementSibling;
                  if(npfFirst && npfFirst.matches(".npf_group:not(.npf-small)")){
                    let znext = npfFirst.nextElementSibling;

                    // no caption
                    if(!znext || znext && znext.innerHTML.trim() == ""){
                      tumblr_parent.classList.add("tbd");
                      npfFirst.classList.add("photo-origin");
                      tumblr_parent.before(npfFirst);
                      tumblr_parent.remove();

                      if(hasCaption == "true"){
                        let capt = textPost.querySelector(captionName);
                        if(capt){
                          npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";
                          capt.classList.add("npf-caption");
                          capt.before(npfFirst);

                          if(otherReblogs == "false"){
                            capt.remove()
                          }
                        }
                      }
                    }//end: no caption

                    // has caption
                    else {
                      npfFirst.classList.add("photo-origin");
                      tumblr_parent.before(npfFirst);

                      if(hasCaption == "true"){
                        let capt = textPost.querySelector(captionName);
                        if(capt){
                          capt.classList.add("npf-caption");
                          capt.before(npfFirst);
                          npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";
                          capt.classList.add("npf-caption");
                        }
                      }//end: if text container var input is non-empty
                    }//end: has caption
                  }//end: if first element is .npf_group
                }//end: if a.tumblr_blog exists
                
                // a.tumblr_blog does not exist,
                // possibly an original post stuck inside .tumblr_parent
                else {
                  clearInterval(snxon)
                  let zprev = tumblr_parent.previousElementSibling;
                  if(!zprev || zprev && zprev.innerHTML.trim() == ""){
                    let znext = tumblr_parent.nextElementSibling;
                    if(!znext || znext && znext.innerHTML.trim() == ""){
                      // is the only case of .tumblr_parent
                      let npfFirst = tumblr_parent.querySelector(".npf_group:not(.npf-small)");
                      let yprev = tumblr_parent.previousElementSibling;
                      if(!yprev || yprev && yprev.innerHTML.trim() == ""){
                        // npfFirst has no prev
                        let ynext = npfFirst.nextElementSibling;
                        
                        // no caption
                        if(!ynext || ynext && ynext.innerHTML.trim() == ""){
                          tumblr_parent.classList.add("npf-caption");
                        }
                        
                        // has caption
                        else {
                          tumblr_parent.classList.add("npf-caption");
                          if(hasCaption == "true"){
                            let textCont = tumblr_parent.closest(captionName);
                            if(textCont){
                              textCont.before(npfFirst);
                              npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";
                            }
                          }
                        }//end: has caption
                      }
                    }//end: .tumblr_parent has no .next();
                  }//end: .tumblr_parent has no .prev();
                }//end: if a.tumblr_blog does not exist
              }//end: if .tumblr_parent exists (in some form)


              // if .tumblr_parent cannot be found
              // likely an original post
              else {
                clearInterval(snxon);

                if(hasCaption == "true"){
                  let capt = textPost.querySelector(captionName);
                  if(capt){
                    capt.classList.add("npf-caption");

                    textPost.querySelectorAll(captionName)?.forEach((textCont, i) => {
                      if(i == 0){
                        setTimeout(() => {
                          let npfFirst = textCont.querySelector(".npf_group:not(.npf-small)");
                          if(npfFirst){
                            let zbefore = npfFirst.previousElementSibling;
                            if(!zbefore || zbefore && zbefore.innerHTML.trim() == ""){
                              npfFirst.classList.add("photo-origin");
                              let znext = npfFirst.nextElementSibling;

                              // no caption
                              if(!znext || znext && znext.innerHTML.trim() == ""){
                                textCont.before(npfFirst);
                                textCont.remove();
                                npfFirst.style.marginBottom = 0;
                                npfFirst.querySelectorAll(":scope > .npf_inst:last-child")?.forEach(n => {
                                  n.style.marginBottom = 0;
                                })
                              }//end: no caption

                              // has caption
                              else {
                                textCont.classList.add("npf-caption");
                                textCont.before(npfFirst);
                                npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";
                              }//end: has caption
                            }//end: if npfFirst is truly the first
                          }//end: if npfFirst exists
                        },0);
                      }//end: first text container in that text post
                    })//end: text container forEach
                  }//end: if text container (el) exists
                }//end: if text container var input is non-empty
              }//end: no .tumblr_parent
            },0)//end: brief delay after unnested is detected
              
          }//end interval (else)
        },0) // was 1
        
        textPost.classList.add("npf-loaded")
      })//end: textpost forEach
    }//end: if unnest captions detected
    
    else {
      document.querySelectorAll("[post-type='text']:not(.npf-loaded)")?.forEach(textPost => {
        let leg = textPost.querySelector("p:first-child > a.tumblr_blog[href]:first-child");
        if(leg && leg.parentNode.nextElementSibling && leg.parentNode.nextElementSibling.matches("blockquote:not([class])")){
          
          /*--------- OLD BLOCKQUOTE CAPTIONS [CASE: 2] ---------*/        
          let lastComment = leg.parentNode;
          lastComment.setAttribute("last-comment","");

          textPost.querySelectorAll("p:first-child > a.tumblr_blog[href]:first-child")?.forEach(a => {
            a.parentNode.setAttribute("username","")
          })

          textPost.querySelectorAll("p[username] + blockquote")?.forEach(b => {
            b.setAttribute("said","")
          })
          
          let reblogs = textPost.querySelector("p[username] + blockquote[said]");
          
          // has reblogs
          if(reblogs){
            textPost.querySelectorAll("p[username] + blockquote[said]")?.forEach(b => {
              if(!b.querySelector("p[username]")){
                // ORIGINAL ENTRY
                let originalEntry = b;
                let npfFirst = originalEntry.querySelector(".npf_group:not(.npf-small)");
                if(npfFirst){                
                  let prev = npfFirst.previousElementSibling;
                  if(!prev || (prev && prev.innerHTML.trim() == "")){
                    // CAN RELOCATE
                    npfFirst.classList.add("photo-origin");

                    let next = npfFirst.nextElementSibling;
                    if(!next || (next && next.innerHTML.trim() == "")){
                      // NO CAPTION
                      originalEntry.classList.add("tbd")
                      originalEntry.previousElementSibling.classList.add("tbd");                  
                    }

                    npfFirst.closest("[post-type='text']").querySelector("p[last-comment]").before(npfFirst);
                    npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";

                    // only 1 comment
                    let nom = npfFirst.nextElementSibling;
                    if(nom.matches(".tbd")){
                      if(nom.nextElementSibling.matches(".tbd")){
                        nom.nextElementSibling.remove()
                        nom.remove();
                      }
                    }

                    // multiple comments
                    else {
                      nom = npfFirst.nextElementSibling.nextElementSibling;
                      if(nom.matches("blockquote")){
                        nom.querySelectorAll(".tbd")?.forEach(tbd => {
                          tbd.remove()
                        })
                      }
                    }

                  }
                }
              }
            })//end: find deepest bq (op)
          }//end: has reblogs
          
          // no reblogs
          // likely an original post
          else {
            // for some reason (I don't remember) this is accounted for
          }//end: no reblogs

          textPost.querySelectorAll("p[username], blockquote[said]")?.forEach(e => {
            e.removeAttribute("username");
            e.removeAttribute("said");
          })
        }//end: OLD BLOCKQUOTE CAPTIONS

        else {
          /*--------- MODERN CAPTIONS [CASE: 3] ---------*/
          
          function doTheThing(input){
            // loop through entries
            textPost.querySelectorAll(input)?.forEach((entry, i) => {
              // focus on the 1st entry, the original entry
              if(i == 0){
                setTimeout(() => {
                  // .npf_inst: first
                  let npfFirst = entry.querySelector(".npf_group:not(.npf-small)"); // already first instance

                  if(npfFirst){
                    /*---- NEXT ----*/
                    let npfNext = npfFirst.nextElementSibling;
                    if(npfNext){
                      if(npfNext.innerHTML.trim() !== ""){
                        // has caption
                        entry.classList.add("npf-caption")
                      } else {
                        // no caption
                        entry.classList.add("tbd")
                      }
                    } else {
                      // no caption
                      entry.classList.add("tbd")
                    }

                    /*---- PREVIOUS ----*/
                    let npfPrev = npfFirst.previousElementSibling;
                    if(npfPrev){
                      if(npfPrev.matches("a[href*='.tumblr.com/post']") || npfPrev.matches("a.user") || npfPrev.matches("a.username") || npfPrev.matches("a[target='_blank']") || (npfPrev.matches("p") && npfPrev.innerHTML.trim() == "")){
                        moveIt()
                      }

                      else if(npfPrev.matches("p")){
                        if(npfPrev.innerHTML.trim() == ""){
                          moveIt()
                        }
                      }

                      else {
                        // do nothing
                      }
                    } else {
                      moveIt()
                    }

                    function moveIt(){
                      npfFirst.classList.add("photo-origin");
                      entry.before(npfFirst);
                      if(!entry.matches(".tbd")){
                        npfFirst.style.marginBottom = "var(--NPF-Caption-Spacing)";
                      }

                      if(hasCaption == "true"){
                        let findCapt = entry.closest(`[post-type="text"]`).querySelector(captionName);
                        findCapt.before(npfFirst);
                        if(entry.matches(".tbd")){
                          // findCapt.remove()
                          let checkNext = entry.nextElementSibling;

                          // if theres a .next() but it's not another reblog/comment
                          if(checkNext){
                            if(!checkNext.matches(input)){
                              findCapt.remove()
                            }
                          }

                          // no .next()
                          else {
                            findCapt.remove();
                          }

                          entry.remove();
                        }
                      } else {
                        if(entry.matches(".tbd")){
                          entry.remove()
                        }
                      }
                    }//end moveIt()
                  }//end: npfFirst exists
                },0)
              }//end: original entry
            })//end: loop through either comments or text containers
          }// end doTheThing()
          
          // has: entry
          if(hasEntry == "true" && textPost.querySelector(entryName)){
            doTheThing(entryName);
          }
          
          // no: entry, has: text container
          else if(hasCaption == "true" && textPost.querySelector(captionName)){
            doTheThing(captionName)
          }
        }//end: MODERN CAPTIONS
        
        textPost.classList.add("npf-loaded")
        
      })//end [post-type="text"] forEach
    }//end: anything thats NOT neobev unnest
    
  }//end: YES, PREPEND IT
}//end NPFv4

/*----- CHECK aspect-ratio COMPATIBILITY -----*/
window.aspectRatioOrUseHeight = () => {
	let supportsAR = "aspect-ratio" in document.documentElement.style;
	if(supportsAR){
		typeof npfRowHeightAR === "function" && npfRowHeightAR();
	} else {
		typeof npfRowHeight === "function" && npfRowHeight();
	}
}

/*-------  NPF ROW HEIGHT: ASPECT-RATIO -------*/
window.npfRowHeightAR = () => {
	document.querySelectorAll(".npf_row:not([data-npf-row-ready])")?.forEach(row => {
		let ratiosEachRow = []; // decimal
		let aspectRatiosEachRow = []; // e.g. 16/9
    
		let colSelector = ".npf_col:not(.npf_col .npf_col)"
		row.querySelectorAll(colSelector)?.forEach(col => {
			let pic = col.querySelector("[data-big-photo-width][data-big-photo-height]");
			if(pic && pic.dataset.bigPhotoWidth.trim() !== "" && pic.dataset.bigPhotoHeight.trim() !== ""){
				let w = Number(pic.dataset.bigPhotoWidth);
				let h = Number(pic.dataset.bigPhotoHeight);
				if(isNaN(w) || isNaN(h)){
					return;
				} else {
					ratiosEachRow.push(w/h) // decimal
					aspectRatiosEachRow.push(`${w}/${h}`) // e.g. 16/9
				}
			}
		})//end col each

		if(ratiosEachRow.length == row.querySelectorAll(colSelector).length){
			let shortest = Math.max(...ratiosEachRow);
			let indexOfShortest = ratiosEachRow.findIndex(x => x === shortest)
			let shortestAspectRatio = aspectRatiosEachRow[indexOfShortest]
			row.querySelectorAll(colSelector)?.forEach(col => col.style.aspectRatio = shortestAspectRatio)
      
      row.dataset.npfRowReady = ""
		}
	})//end row each
}//end npfRowHeightAR

/*------ NPF ROW HEIGHT: USE PADDED BOX ------*/
// note: it's not actually to do with the offsetHeight but the function was already called that before
window.npfRowHeight = () => {
  document.querySelectorAll(".npf_row[columns]:not([data-npf-row-ready], [columns='1'], [columns=''], [columns='0'])")?.forEach(row => {
    // HOW MANY COLUMNS IN THAT ROW?
    let colsNum = row.querySelectorAll(":scope > .npf_col").length;
    
    // NATURAL DIMENSIONS (ORIGINAL DIMENSIONS)
    let NATURAL_WIDTHS = [];
    let NATURAL_HEIGHTS = [];
    
    // COLLECT RATIOS (W/H)
    let NATURAL_RATIOS = [];
    
    // height after img confirmed loaded
    row.querySelectorAll(":scope > .npf_col img:first-of-type")?.forEach(img => {
      let w,h,r;
      
      // image already loaded, get dims
      if(img.complete){
        w = img.naturalWidth;
        h = img.naturalHeight;
        r = w/h;
        
        addDims(w,h,r);
      }
      
      // image NOT YET loaded, use load evt, then get dims
      else {
        img.addEventListener("load", e => {
          w = e.target.naturalWidth;
          h = e.target.naturalHeight;
          r = w/h;
          
          addDims(w,h,r);
        })
      }
      
      // do stuff with the dims
      function addDims(w,h,r){
        NATURAL_WIDTHS.push(w);
        NATURAL_HEIGHTS.push(h);        
        NATURAL_RATIOS.push(r)
        
        // should only trigger once per row
        if(NATURAL_WIDTHS.length == colsNum){
          // ratios: higher number = shorter
          let shortest_val = Math.max(...NATURAL_RATIOS);
          let shortest_dex = NATURAL_RATIOS.findIndex(x => x == shortest_val);
          
          // using pic index, retrieve that img's dimensions
          let sw = NATURAL_WIDTHS[shortest_dex];
          let sh = NATURAL_HEIGHTS[shortest_dex];
          
          // get the % for padded box
          let sp = sh/sw * 100;

          row.style.setProperty("--npf-img-pct",`${sp}%`);
          row.dataset.npfHeight = ""
          row.dataset.npfRowReady = ""
        }//end: collected all dims of imgs in that row
      }//end addDims()
      
    })//end img each
    
  })//end row each
}//end: npfRowHeight

document.addEventListener("DOMContentLoaded", () => {
  NPFv4();
  
  if(window.jQuery || window.$){
    if($().infinitescroll){
      let hi = Math.max(document.documentElement.scrollHeight || 0, document.body.scrollHeight || 0);
      window.addEventListener("scroll", () => {
        let height_observed = Math.max(document.documentElement.scrollHeight || 0, document.body.scrollHeight || 0);
        let diff = height_observed - hi;
        if(diff > 100){
          NPFv4();
        }
      })
    }
  }
})