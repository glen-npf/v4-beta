NPF v4 Fix: CHANGELOG

> All changes are listed from *oldest* to *newest*.

- issue scope: modern captions
  issue details: captionless npf would delete comments
  example post: [glen-test.tumblr.com/737458089298722816](https://glen-test.tumblr.com/737458089298722816)
- issue scope: row/column heights
  issue details: instantiated img loads incorrectly
- issue scope: `figure:not(.tmblr-full)`
  issue details: should have this class but doesn't
  issue update: now only applies to `[post-type] *`
- added `npf_photo` class to npf photo(set)s
- added bot-gap for unnested captions without original captions
- added bot-gap for old blockquote captions
- accounted for `.photo-origin`(s) in original posts (I somehow forgot them)
- improvements for unnested capts (original posts)
- prevent prepending 1st photoset IF under width threshold [static.tumblr.com/gtjt4bo/fUqsl6252/inline-image-sizes.png](https://static.tumblr.com/gtjt4bo/fUqsl6252/inline-image-sizes.png)
- now uses `aspect-ratio` to adopt shortest image height of that row, unless the browser doesn't support it
- perf: in case the browser has no `aspect-ratio` support, row height has been changed to the [padded box](http://daverupert.com/2012/04/uncle-daves-ol-padded-box/) (padding-bottom) method instead of relying on `offsetHeight` and `resize` events